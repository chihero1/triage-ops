# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/constants/labels'
require_relative '../../triage/processor/assign_dev_for_verification'
require_relative '../../triage/triage/event'

RSpec.describe Triage::AssignDevForVerification do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:label_names) { ['workflow::verification', 'group::security policies', 'backend'] }
    let(:project_id) { 12 }
    let(:issue_iid) { 1234 }
    let(:event_attrs) do
      {
        project_id: project_id,
        iid: issue_iid,
        from_gitlab_org?: true,
        assignee_ids: [],
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of gitlab org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when group and workflow labels are not in label_names' do
      before do
        allow(event).to receive(:label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when issue is assigned' do
      before do
        allow(event).to receive(:assignee_ids).and_return([1])
      end

      include_examples 'event is not applicable'
    end

    context 'when labels are not valid' do
      before do
        allow(event).to receive(:label_names).and_return(["workflow::refinement"])
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:security_policies_be) { '@security_policies_be' }
    let(:threat_insights_be) { '@threat_insights_be' }
    let(:threat_insights_fe) { '@threat_insights_fe' }

    before do
      allow(subject).to receive(:security_policies_be).and_return(security_policies_be)
      allow(subject).to receive(:threat_insights_be).and_return(threat_insights_be)
      allow(subject).to receive(:threat_insights_fe).and_return(threat_insights_fe)
      allow(subject).to receive(:sleep)
    end

    shared_examples 'processes event' do |dev_username|
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: []
        }
      end

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          This issue is ready to be verified and according to our [verification process](https://about.gitlab.com/handbook/engineering/development/sec/govern/planning/#verification)
          we need your help with this activity.

          #{dev_username}, would you mind taking a look if this issue can be verified on production and close this issue?

          /assign #{dev_username}
        MARKDOWN

        expect_api_requests do |requests|
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue)
          requests << stub_comment_request(event: event, body: body)

          subject.process
        end
      end
    end

    context 'when issue belongs to security policies' do
      let(:label_names) { ['workflow::verification', 'group::security policies', 'backend'] }

      it_behaves_like 'processes event', '@security_policies_be'
    end

    context 'when issue belongs to threat insights frontend' do
      let(:label_names) { ['workflow::verification', Labels::THREAT_INSIGHTS_GROUP_LABEL, 'frontend'] }

      it_behaves_like 'processes event', '@threat_insights_fe'
    end

    context 'when issue belongs to threat insights backend' do
      let(:label_names) { ['workflow::verification', Labels::THREAT_INSIGHTS_GROUP_LABEL, 'backend'] }

      it_behaves_like 'processes event', '@threat_insights_be'
    end

    context 'when group label is different' do
      let(:label_names) { ['workflow::verification', 'group::composition analysis', 'backend'] }

      it 'raises exception' do
        expect { subject.process }.to raise_error(Triage::AssignDevForVerification::InvalidLabelsError)
      end
    end

    context 'when there is assignee' do
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: [{ id: 1 }]
        }
      end

      it 'does not assign dev' do
        expect_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue) do
          subject.process
        end
      end
    end
  end
end
