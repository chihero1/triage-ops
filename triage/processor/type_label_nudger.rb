# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/markdown_collapsible'
require_relative '../job/type_label_nudger_job'
require_relative '../../lib/constants/labels'

module Triage
  class TypeLabelNudger < Processor
    FIVE_MINUTES                 = 300
    COMMUNITY_CONTRIBUTION_LABEL = Labels::COMMUNITY_CONTRIBUTION_LABEL
    COLLAPSIBLE_COMMENT_TITLE    = 'Proper labels assigned to this merge request. Please ignore me.'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.resource_open? &&
        event.from_part_of_product_project? &&
        !event.gitlab_bot_event_actor? &&
        !community_contribution? &&
        (need_to_nudge? || need_to_resolve_nudge?)
    end

    # We initially give the author a few minutes to add a type and subtype label before we post a reminder.
    #
    # Resolving/unresolving discussions, on the other hand, is instantaneous.
    def process
      if need_to_nudge?
        if need_to_unresolve_nudge?
          update_discussion_comment(collapsible_removed_message, previous_discussion['id'], previous_discussion_comment['id']) if collapsible_comment_present?
          unresolve_discussion(previous_discussion['id'])
        else
          TypeLabelNudgerJob.perform_in(FIVE_MINUTES, event)
        end
      elsif need_to_resolve_nudge?
        update_discussion_comment(collapsible_message.collapsed_content, previous_discussion['id'], previous_discussion_comment['id']) unless collapsible_comment_present?
        resolve_discussion(previous_discussion['id'])
      end
    end

    def documentation
      <<~TEXT
        Reminds the MR creator to add a type and subtype label to the MR.
      TEXT
    end

    private

    def previous_discussion
      unique_comment.previous_discussion
    end

    def previous_discussion_comment
      unique_comment.previous_discussion_comment
    end

    def community_contribution?
      event.label_names.include?(Labels::COMMUNITY_CONTRIBUTION_LABEL)
    end

    def need_to_nudge?
      (!valid_type_label? || !subtype_label_present?) &&
        (!unique_comment.previous_discussion_comment || unique_comment.previous_discussion_comment['resolved'])
    end

    def need_to_resolve_nudge?
      valid_type_label? &&
        subtype_label_present? &&
        unique_comment.previous_discussion_comment &&
        !unique_comment.previous_discussion_comment['resolved']
    end

    def need_to_unresolve_nudge?
      !valid_type_label? && unique_comment.previous_discussion
    end

    def valid_type_label?
      type_label_present? && !type_ignore_label_present?
    end

    def type_label_present?
      (event.label_names & Labels::TYPE_LABELS).any?
    end

    def type_ignore_label_present?
      event.label_names.include?(Labels::TYPE_IGNORE_LABEL)
    end

    def subtype_label_present?
      event.label_names.any? { |label| label.start_with?('bug::', 'feature::', 'maintenance::') }
    end

    def collapsible_comment_present?
      previous_discussion_comment['body'].include?(COLLAPSIBLE_COMMENT_TITLE)
    end

    def collapsible_message
      @collapsible_message ||= Triage::MarkdownCollapsible.new(COLLAPSIBLE_COMMENT_TITLE, previous_discussion_comment['body'])
    end

    def collapsible_removed_message
      collapsible_message.uncollapsed_content
    end
  end
end
