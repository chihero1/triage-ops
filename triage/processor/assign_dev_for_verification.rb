# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/team_member_select_helper'
require_relative '../../lib/constants/labels'

module Triage
  class AssignDevForVerification < Processor
    include TeamMemberSelectHelper

    VERIFICATION_LABEL = 'workflow::verification'
    GROUP_SECURITY_POLICIES_LABEL = 'group::security policies'
    GROUP_THREAT_INSIGHTS_LABEL = Labels::THREAT_INSIGHTS_GROUP_LABEL
    BACKEND_LABEL = Labels::BACKEND_LABEL
    FRONTEND_LABEL = Labels::FRONTEND_LABEL
    InvalidLabelsError = Class.new(StandardError)

    react_to 'issue.update'

    def applicable?
      event.from_gitlab_org? &&
        empty_assignee? &&
        has_valid_labels?
    end

    def process
      team_member_to_assign

      sleep(rand * 3) # Attempt to reduce race

      assign_team_member_for_verification if still_no_assignee?
    end

    def documentation
      <<~TEXT
      This processor assigns a dev from the configured group to the updated issue
      when the issue does not have any assignees and moved to `workflow::verification`.
      TEXT
    end

    private

    def has_valid_labels?
      has_verification_label? && has_group_label? && has_component_label?
    end

    def has_verification_label?
      label_names.include?(VERIFICATION_LABEL)
    end

    def has_group_label?
      ([GROUP_SECURITY_POLICIES_LABEL, GROUP_THREAT_INSIGHTS_LABEL] & label_names).any?
    end

    def has_component_label?
      ([BACKEND_LABEL, FRONTEND_LABEL] & label_names).any?
    end

    def empty_assignee?
      event.assignee_ids.empty?
    end

    def still_no_assignee?
      issue = Triage.api_client.issue(event.project_id, event.iid)
      issue.present? && issue.assignees.empty?
    end

    def team_member_to_assign
      @team_member_to_assign ||=
        if label_names.include?(GROUP_SECURITY_POLICIES_LABEL)
          security_policies_be
        elsif label_names.include?(GROUP_THREAT_INSIGHTS_LABEL)
          if label_names.include?(BACKEND_LABEL)
            threat_insights_be
          elsif label_names.include?(FRONTEND_LABEL)
            threat_insights_fe
          end
        else
          raise InvalidLabelsError, 'The issue does not contain required labels to assign a team member'
        end
    end

    def label_names
      @label_names ||= event.label_names
    end

    def assign_team_member_for_verification
      comment = <<~MARKDOWN.chomp
        This issue is ready to be verified and according to our [verification process](https://about.gitlab.com/handbook/engineering/development/sec/govern/planning/#verification)
        we need your help with this activity.

        #{team_member_to_assign}, would you mind taking a look if this issue can be verified on production and close this issue?

        /assign #{team_member_to_assign}
      MARKDOWN
      add_comment(comment, append_source_link: false)
    end
  end
end
